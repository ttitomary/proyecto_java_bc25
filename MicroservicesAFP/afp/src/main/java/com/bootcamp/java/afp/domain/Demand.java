package com.bootcamp.java.afp.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.bootcamp.java.afp.web.DemandMount;
import com.bootcamp.java.afp.web.DemandMountMin;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Demand {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Afp afp;
	
	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Client client;
	
	@NotNull
	@Column(unique = true)
	@Pattern(regexp = "^[1-9]+[0-9]*$")
	private String document;

	@DemandMount
	@DemandMountMin
	@NotNull
	private double mount;
	
	private char status;
}
