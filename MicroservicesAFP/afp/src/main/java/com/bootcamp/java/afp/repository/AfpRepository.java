package com.bootcamp.java.afp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.java.afp.domain.Afp;

public interface AfpRepository extends JpaRepository<Afp, Long> {

}
