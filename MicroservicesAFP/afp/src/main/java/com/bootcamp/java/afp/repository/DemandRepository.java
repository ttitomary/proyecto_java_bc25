package com.bootcamp.java.afp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.java.afp.domain.Demand;

public interface DemandRepository extends JpaRepository<Demand, Long>{

}
