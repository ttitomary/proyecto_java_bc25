package com.bootcamp.java.afp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bootcamp.java.afp.domain.Detail;

public interface DetailRepository extends JpaRepository<Detail, Long>{

}
