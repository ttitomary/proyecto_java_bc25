package com.bootcamp.java.afp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bootcamp.java.afp.domain.Afp;
import com.bootcamp.java.afp.repository.AfpRepository;
import com.bootcamp.java.afp.service.mapper.AfpMapper;
import com.bootcamp.java.afp.web.model.AfpModel;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class AfpService implements IAfpService{
	
	private final AfpRepository afpRepository;
	private final AfpMapper afpMapper;
	
	@Override
	public List<AfpModel> findAll() throws Exception {
		List<Afp> afps = afpRepository.findAll();
		return afpMapper.afpsToAfpModels(afps);
	}
	
	@Override
	public AfpModel findById(Long id) throws Exception {
		Optional<Afp> afp = afpRepository.findById(id);
		if(afp.isPresent()) return afpMapper.afpToAfpModel(afp.get());
		else throw new Exception("No se encontraron datos");
	}
	
	@Override
	public void update(Long id, AfpModel afpModel) throws Exception {
		Optional<Afp> afpOptional = afpRepository.findById(id);
		
		if(afpOptional.isPresent()) {
			Afp afpToUpdateAfp = afpOptional.get();
			afpMapper.update(afpToUpdateAfp, afpModel);
			afpRepository.save(afpToUpdateAfp);
		}
		else throw new Exception("No se encontraron datos");
	}

	@Override
	public AfpModel create(AfpModel afpModel) throws Exception {
		Afp afp = afpRepository.save(afpMapper.afpModelToAfp(afpModel));
		return afpMapper.afpToAfpModel(afp);
	}

	@Override
	public void deleteById(Long id) throws Exception {
		afpRepository.deleteById(id);
	}
}
