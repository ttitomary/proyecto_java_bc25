package com.bootcamp.java.afp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bootcamp.java.afp.domain.Client;
import com.bootcamp.java.afp.repository.ClientRepository;
import com.bootcamp.java.afp.service.mapper.ClientMapper;
import com.bootcamp.java.afp.web.model.ClientModel;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class ClientService implements IClientService{

	private final ClientRepository clientRepository;
	private final ClientMapper clientMapper;
	
	@Override
	public List<ClientModel> findAll() throws Exception {
		List<Client> clients = clientRepository.findAll();
		return clientMapper.clientsToClienteModels(clients);
	}
	
	@Override
	public ClientModel findById(Long id) throws Exception {
		Optional<Client> client = clientRepository.findById(id);
		if(client.isPresent()) return clientMapper.clientToClientModel(client.get());
		else throw new Exception("No se encontraron datos");
	}
	
	@Override
	public ClientModel create(ClientModel clientModel) throws Exception {
		Client client = clientRepository.save(clientMapper.clientModelToClient(clientModel));
		return clientMapper.clientToClientModel(client);
	}
	
	@Override
	public void update(Long id, ClientModel clientModel) throws Exception {
		Optional<Client> clientOptional = clientRepository.findById(id);
		
		if(clientOptional.isPresent()) {
			Client clientToUpdateClient = clientOptional.get();
			clientMapper.update(clientToUpdateClient, clientModel);
			clientRepository.save(clientToUpdateClient);
		}
		else throw new Exception("No se encontraron datos");
	}
	
	@Override
	public void deleteById(Long id) throws Exception {
		clientRepository.deleteById(id);
	}
}
