package com.bootcamp.java.afp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bootcamp.java.afp.domain.Demand;
import com.bootcamp.java.afp.repository.DemandRepository;
import com.bootcamp.java.afp.service.mapper.DemandMapper;
import com.bootcamp.java.afp.web.model.DemandModel;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DemandService implements IDemandService {

	private final DemandRepository demandRepository;
	private final DemandMapper demandMapper;
	
	@Override
	public List<DemandModel> findAll() throws Exception {
		List<Demand> demands = demandRepository.findAll();
		return demandMapper.demandsToDemandModels(demands);
	}
	
	@Override
	public DemandModel findById(Long id) throws Exception {
		Optional<Demand> demand = demandRepository.findById(id);
		if(demand.isPresent()) return demandMapper.demandToDemandModel(demand.get());
		else throw new Exception("No se encontraron datos");
	}
	
	@Override
	public DemandModel create(DemandModel demandModel) throws Exception {
		Long id = demandModel.getClient().getId();
		Optional<Demand> demandOptional = demandRepository.findAll().stream().filter(s -> id.equals(s.getClient().getId())).findFirst();
		
		if(!demandOptional.isPresent()) {
			Demand demand = demandRepository.save(demandMapper.demandModelToDemand(demandModel));
			return demandMapper.demandToDemandModel(demand);
		}
		else throw new Exception("Cliente ya tiene una solicitud de retiro");
	}
	
	@Override
	public void update(Long id, DemandModel demandModel) throws Exception {
		Optional<Demand> demandOptional = demandRepository.findById(id);
		
		if(demandOptional.isPresent()) {
			Demand demandToUpdateDemand = demandOptional.get();
			demandMapper.update(demandToUpdateDemand, demandModel);
			demandRepository.save(demandToUpdateDemand);
		}
		else throw new Exception("No se encontraron datos");
	}
	
	@Override
	public void deleteById(Long id) throws Exception {
		demandRepository.deleteById(id);
	}
	
}
