package com.bootcamp.java.afp.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.bootcamp.java.afp.domain.Detail;
import com.bootcamp.java.afp.repository.DetailRepository;
import com.bootcamp.java.afp.service.mapper.DetailMapper;
import com.bootcamp.java.afp.web.model.DetailModel;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class DetailService implements IDetailService{

	private final DetailRepository detailRepository;
	private final DetailMapper detailMapper;
	
	@Override
	public List<DetailModel> findAll() throws Exception {
		List<Detail> details = detailRepository.findAll();
		return detailMapper.detailsToDetailModels(details);
	}
	@Override
	public DetailModel findById(Long id) throws Exception {
		Optional<Detail> detail = detailRepository.findById(id);
		if(detail.isPresent()) return detailMapper.detailToDetailModel(detail.get());
		else throw new Exception("No se encontraron datos");
	}
	@Override
	public DetailModel create(DetailModel detailModel) throws Exception {
		Long id = detailModel.getClient().getId();
		Optional<Detail> detailOptional = detailRepository.findAll().stream().filter(s -> id.equals(s.getClient().getId())).findFirst();
		
		if(!detailOptional.isPresent()) {
			Detail detail = detailRepository.save(detailMapper.detailModelToDetail(detailModel));
			return detailMapper.detailToDetailModel(detail);
		}
		else throw new Exception("Cliente ya tiene afiliacion");
		
	}
	@Override
	public void update(Long id, DetailModel detailModel) throws Exception {
		Optional<Detail> detailOptional = detailRepository.findById(id);
		
		if(detailOptional.isPresent()) {
			Detail detailToUpdateDetail = detailOptional.get();
			detailMapper.update(detailToUpdateDetail, detailModel);
			detailRepository.save(detailToUpdateDetail);
		}
		else throw new Exception("No se encontraron datos");
	}
	@Override
	public void deleteById(Long id) throws Exception {
		detailRepository.deleteById(id);
		
	}
}
