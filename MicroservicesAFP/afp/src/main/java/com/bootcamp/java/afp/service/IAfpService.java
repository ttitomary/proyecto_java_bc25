package com.bootcamp.java.afp.service;

import java.util.List;

import com.bootcamp.java.afp.web.model.AfpModel;

public interface IAfpService {
	List<AfpModel> findAll() throws Exception;
	AfpModel findById(Long id) throws Exception;
	AfpModel create(AfpModel afpModel) throws Exception;
	void update(Long id, AfpModel afpModel) throws Exception;
	void deleteById(Long id) throws Exception;

}
