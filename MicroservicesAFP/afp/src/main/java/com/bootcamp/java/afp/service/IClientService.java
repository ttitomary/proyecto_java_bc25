package com.bootcamp.java.afp.service;

import java.util.List;

import com.bootcamp.java.afp.web.model.ClientModel;

public interface IClientService {
	List<ClientModel> findAll() throws Exception;
	ClientModel findById(Long id) throws Exception;
	ClientModel create(ClientModel clientModel) throws Exception;
	void update(Long id, ClientModel clientModel) throws Exception;
	void deleteById(Long id) throws Exception;

}
