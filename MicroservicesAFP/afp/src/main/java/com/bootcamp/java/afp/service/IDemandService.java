package com.bootcamp.java.afp.service;

import java.util.List;

import com.bootcamp.java.afp.web.model.DemandModel;

public interface IDemandService {
	List<DemandModel> findAll() throws Exception;
	DemandModel findById(Long id) throws Exception;
	DemandModel create(DemandModel demandModel) throws Exception;
	void update(Long id, DemandModel demandModel) throws Exception;
	void deleteById(Long id) throws Exception;
}
