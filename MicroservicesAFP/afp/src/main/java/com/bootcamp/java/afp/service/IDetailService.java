package com.bootcamp.java.afp.service;

import java.util.List;

import com.bootcamp.java.afp.web.model.DetailModel;

public interface IDetailService {
	List<DetailModel> findAll() throws Exception;
	DetailModel findById(Long id) throws Exception;
	DetailModel create(DetailModel detailModel) throws Exception;
	void update(Long id, DetailModel detailModel) throws Exception;
	void deleteById(Long id) throws Exception;
}
