package com.bootcamp.java.afp.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import com.bootcamp.java.afp.domain.Client;
import com.bootcamp.java.afp.web.model.ClientModel;

@Mapper(componentModel = "spring")
public interface ClientMapper {
	Client clientModelToClient(ClientModel model);
	ClientModel clientToClientModel (Client domain);
	List<ClientModel> clientsToClienteModels(List<Client> domain);
	
	@Mapping(target = "id", ignore = true)
	void update(@MappingTarget Client entity, ClientModel updateEntity);
}
