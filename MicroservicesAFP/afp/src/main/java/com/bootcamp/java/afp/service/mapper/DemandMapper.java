package com.bootcamp.java.afp.service.mapper;
import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import com.bootcamp.java.afp.domain.Demand;
import com.bootcamp.java.afp.web.model.DemandModel;

@Mapper(componentModel = "spring")
public interface DemandMapper {
	Demand demandModelToDemand(DemandModel model);

	DemandModel demandToDemandModel(Demand domain);

	List<DemandModel> demandsToDemandModels(List<Demand> domain);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "afp", ignore = true)
	@Mapping(target = "client", ignore = true)
	@Mapping(target = "document", ignore = true)
	@Mapping(target = "status", ignore = true)
	void update(@MappingTarget Demand entity, DemandModel updateEntity);
	
}
