package com.bootcamp.java.afp.service.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import com.bootcamp.java.afp.domain.Detail;
import com.bootcamp.java.afp.web.model.DetailModel;

@Mapper(componentModel = "spring")
public interface DetailMapper {

	Detail detailModelToDetail(DetailModel model);

	DetailModel detailToDetailModel(Detail domain);

	List<DetailModel> detailsToDetailModels(List<Detail> domain);

	@Mapping(target = "id", ignore = true)
	@Mapping(target = "afp", ignore = true)
	@Mapping(target = "client", ignore = true)
	@Mapping(target = "document", ignore = true)
	void update(@MappingTarget Detail entity, DetailModel updateEntity);
}
