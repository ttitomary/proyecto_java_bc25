package com.bootcamp.java.afp.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.java.afp.service.IAfpService;
import com.bootcamp.java.afp.web.model.AfpModel;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/afp")
@Slf4j
public class AfpController {
	
	private final IAfpService afpService;

	/**
	 * Metodo que devuelve todas las afp
	 * @return
	 * @throws Exception
	 */
	@GetMapping()
	@Operation(summary = "Get list of afps")
	public ResponseEntity<Object> getAll() throws Exception{
		List<AfpModel> response = afpService.findAll();
		log.info("Metodo getAll:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Metodo que devuelve una afp por el ID
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Get afp by id")
	public ResponseEntity<Object> getById(@PathVariable("id") Long id) throws Exception{
		AfpModel response = afpService.findById(id);
		log.info("Metodo getById:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	

	/**
	 * Metodo que permite crear una afp
	 * @param demandModel
	 * @return
	 * @throws Exception
	 */
	@PostMapping
	@Operation(summary = "Create afp")
	public ResponseEntity<Object> create(@Valid @RequestBody AfpModel afpModel) throws Exception{
		AfpModel response = afpService.create(afpModel);
		log.info("Metodo create:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Metodo con el que podemos actualizar una afp
	 * @param id
	 * @param demandModel
	 * @throws Exception
	 */
	@PutMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Update afp by id")
	public void update(@PathVariable("id") Long id,
					   @RequestBody AfpModel afpModel) throws Exception {
		afpService.update(id, afpModel);	
		log.info("Metodo update:" + "Operacion correcta");
		log.debug(id.toString() + "/" + afpModel.toString());
	}

	/**
	 * Metodo que permite eliminar una afp
	 * @param id
	 * @throws Exception
	 */
	@DeleteMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Delete afp by id")
	public void deleteById(@PathVariable("id") Long id)  throws Exception {
		afpService.deleteById(id);		
		log.info("Metodo deleteById:" + "Operacion correcta");
		log.debug(id.toString());	
	}
	
}
