package com.bootcamp.java.afp.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.java.afp.service.IClientService;
import com.bootcamp.java.afp.web.model.ClientModel;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/client")
@Slf4j
public class ClientController {
	
	private final IClientService clientService;
	
	/**
	 * Metodo que obtendra todos los clientes
	 * @return
	 * @throws Exception
	 */
	@GetMapping()
	@Operation(summary = "Get list of clients")
	public ResponseEntity<Object> getAll()throws Exception{
		List<ClientModel> response = clientService.findAll();
		log.info("Metodo getAll:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Metodo que obtiene el dato del cliente por ID
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Get client by id")
	public ResponseEntity<Object> getById(@PathVariable("id") Long id) throws Exception{
		ClientModel response = clientService.findById(id);
		log.info("Metodo getById:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Metodo que se utiliza para crear un nuevo cliente
	 * @param clientModel
	 * @return
	 * @throws Exception
	 */
	@PostMapping
	@Operation(summary = "Create client")
	public ResponseEntity<Object> create(@Valid @RequestBody ClientModel clientModel) throws Exception{
		ClientModel response = clientService.create(clientModel);
		log.info("Metodo create:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Metodo que actualiza la informacion del cliente
	 * @param id
	 * @param clientModel
	 * @throws Exception
	 */
	@PutMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Update client by id")
	public void update(@PathVariable("id") Long id, 
					   @RequestBody ClientModel clientModel) throws Exception {
		clientService.update(id, clientModel);	
		log.info("Metodo update:" + "Operacion correcta");
		log.debug(id.toString() + "/" + clientModel.toString());
	}
	
	/**
	 * Metodo que elimina el cliente
	 * @param id
	 * @throws Exception
	 */
	@DeleteMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Delete client by id")
	public void deleteById(@PathVariable("id") Long id)  throws Exception {
		clientService.deleteById(id);		
		log.info("Metodo deleteById:" + "Operacion correcta");
		log.debug(id.toString());
	}
}
