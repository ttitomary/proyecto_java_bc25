package com.bootcamp.java.afp.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.java.afp.service.IDemandService;
import com.bootcamp.java.afp.web.model.DemandModel;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/demand")
@Slf4j
public class DemandController {
	
	private final IDemandService demandService;
	
	/**
	 * Metodo que devuelve todas las solicitudes
	 * @return
	 * @throws Exception
	 */
	@GetMapping()
	@Operation(summary = "Get list of demands")
	public ResponseEntity<Object> getAll() throws Exception{
		List<DemandModel> response = demandService.findAll();
		log.info("Metodo getAll:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Metodo que devuelve una solicitud por el ID
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Get demand by id")
	public ResponseEntity<Object> getById(@PathVariable("id") Long id) throws Exception{
		DemandModel response = demandService.findById(id);
		log.info("Metodo getById:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Metodo que permite crear un solicitud
	 * @param demandModel
	 * @return
	 * @throws Exception
	 */
	@PostMapping
	@Operation(summary = "Create demand")
	public ResponseEntity<Object> create(@Valid @RequestBody DemandModel demandModel) throws Exception{
		DemandModel response = demandService.create(demandModel);
		log.info("Metodo create:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Metodo con el que podemos actualizar una solicitud
	 * @param id
	 * @param demandModel
	 * @throws Exception
	 */
	@PutMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Update demand by id")
	public void update(@PathVariable("id") Long id, 
					   @RequestBody DemandModel demandModel) throws Exception {
		demandService.update(id, demandModel);	
		log.info("Metodo update:" + "Operacion correcta");
		log.debug(id.toString() + "/" + demandModel.toString());
	}
	
	/**
	 * Metodo que permite eliminar una solicitud
	 * @param id
	 * @throws Exception
	 */
	@DeleteMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Delete demand by id")
	public void deleteById(@PathVariable("id") Long id)  throws Exception {
		demandService.deleteById(id);		
		log.info("Metodo deleteById:" + "Operacion correcta");
		log.debug(id.toString());	
	}
	
}
