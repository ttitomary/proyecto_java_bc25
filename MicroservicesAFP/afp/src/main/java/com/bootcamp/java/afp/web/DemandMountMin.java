package com.bootcamp.java.afp.web;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;
 
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
 
import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = DemandValidatorMin.class)
@Documented
public @interface DemandMountMin {
	String message () default "No se puede registrar la solicitud. Monto mayor que el permitido";

    Class<?>[] groups () default {};

    Class<? extends Payload>[] payload () default {};
}
