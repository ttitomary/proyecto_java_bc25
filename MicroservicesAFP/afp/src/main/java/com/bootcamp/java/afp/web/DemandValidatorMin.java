package com.bootcamp.java.afp.web;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class DemandValidatorMin implements ConstraintValidator<DemandMountMin, Double>{

	@Override
	public boolean isValid(Double value, ConstraintValidatorContext context) {

		return value > 1000 ? false : true;
	}

}
