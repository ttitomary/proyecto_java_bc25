package com.bootcamp.java.afp.web;

import java.util.List;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bootcamp.java.afp.service.IDetailService;
import com.bootcamp.java.afp.web.model.DetailModel;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/detail")
@Slf4j
public class DetailController {

	private final IDetailService detailService;
	
	/**
	 * Metodo que obtendra el detalle de relacion entre cliente y afp
	 * @return
	 * @throws Exception
	 */
	@GetMapping()
	@Operation(summary = "Get list of detail client/afp")
	public ResponseEntity<Object> getAll()throws Exception{
		List<DetailModel> response = detailService.findAll();
		log.info("Metodo getAll:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	/**
	 * Metodo que obtiene el detalle del cliente/afp por ID
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@GetMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Get client/afp by id")
	public ResponseEntity<Object> getById(@PathVariable("id") Long id) throws Exception{
		DetailModel response = detailService.findById(id);
		log.info("Metodo getById:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Metodo que se utiliza para crear una relacion entre cliente/afp
	 * @param clientModel
	 * @return
	 * @throws Exception
	 */
	@PostMapping
	@Operation(summary = "Create client/afp")
	public ResponseEntity<Object> create(@Valid @RequestBody DetailModel detailModel) throws Exception{
		DetailModel response = detailService.create(detailModel);
		log.info("Metodo create:" + "Operacion correcta");
		log.debug(response.toString());
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
	
	/**
	 * Metodo que actualiza la informacion del cliente/afp
	 * @param id
	 * @param clientModel
	 * @throws Exception
	 */
	@PutMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Update client/afp by id")
	public void update(@PathVariable("id") Long id, 
					   @RequestBody DetailModel detailModel) throws Exception {
		detailService.update(id, detailModel);	
		log.info("Metodo update:" + "Operacion correcta");
		log.debug(id.toString() + "/" + detailModel.toString());
	}
	
	/**
	 * Metodo que elimina el cliente/afp
	 * @param id
	 * @throws Exception
	 */
	@DeleteMapping(path= {"{id}"}, produces = {"application/json"})
	@Operation(summary = "Delete client/afp by id")
	public void deleteById(@PathVariable("id") Long id)  throws Exception {
		detailService.deleteById(id);		
		log.info("Metodo deleteById:" + "Operacion correcta");
		log.debug(id.toString());
	}
}
