package com.bootcamp.java.afp.web.model;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AfpModel {
	@JsonProperty("afpId")
	private Long id;
	
	@NotNull
	@NotBlank(message = "Debe ingresar la descripcion de la AFP")
	private String description;
}
