package com.bootcamp.java.afp.web.model;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientModel {
	@JsonProperty("clientId")
	private Long id;
	
	@NotNull
	@NotBlank(message = "Debe ingresar el nombre del cliente")
	private String name;

	@NotNull
	@NotBlank(message = "Debe ingresar el apellido del cliente")
	private String address;

	@NotNull
	@NotBlank(message = "Debe ingresar el numero de documento del cliente")
	@Pattern(regexp = "^[1-9]+[0-9]*$", message = "Debe ingresar solo numeros")
	private String document;
	
	private String phone;
	private String email;
}
