package com.bootcamp.java.afp.web.model;
import javax.persistence.Column;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.bootcamp.java.afp.web.DemandMount;
import com.bootcamp.java.afp.web.DemandMountMin;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DemandModel {
	
	@JsonProperty("demandId")
	private Long id;
	
	private AfpModel afp;
	
	private ClientModel client;

	@NotNull
	@NotBlank(message = "Debe ingresar el documento del cliente")
	@Column(unique = true)
	@Pattern(regexp = "^[1-9]+[0-9]*$", message = "Debe ingresar solo numeros")
	private String document;

	@DemandMount
	@DemandMountMin
	@NotNull(message = "Debe ingresar el monto del cliente")
	private double mount;
	
	private char status;
}
