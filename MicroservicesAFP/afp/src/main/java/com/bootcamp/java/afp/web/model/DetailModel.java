package com.bootcamp.java.afp.web.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DetailModel {
	@JsonProperty("detailId")
	private Long id;
	
	private AfpModel afp;
	
	private ClientModel client;
	
	@NotNull
	@NotBlank(message = "Debe ingresar el documento del cliente")
	@Column(unique = true)
	@Pattern(regexp = "^[1-9]+[0-9]*$", message = "Debe ingresar solo numeros")
	private String document;
	
	@NotNull(message = "Debe ingresar el monto del cliente")
	private double mount;
	
	@JsonProperty("date")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
	@Future
	@NotNull(message = "La fecha de retiro no debe ser vacia")
	private Timestamp retirement_date;
	
	@NotNull
	@NotBlank(message = "Debe ingresar el numero de cuenta del cliente")
	private String account_number;
}
