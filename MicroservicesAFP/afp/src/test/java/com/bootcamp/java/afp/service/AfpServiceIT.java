package com.bootcamp.java.afp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import com.bootcamp.java.afp.domain.Afp;
import com.bootcamp.java.afp.repository.AfpRepository;
import com.bootcamp.java.afp.service.mapper.AfpMapper;
import com.bootcamp.java.afp.web.model.AfpModel;


@SpringBootTest(classes = {AfpMapper.class})
class AfpServiceIT {
	@Mock
	AfpRepository afpRepository;
	
	@Spy
	private AfpMapper afpMapper = Mappers.getMapper(AfpMapper.class);
	 
	AfpService afpService;
	
	@BeforeEach
	void beforeEach() {
		afpService = new AfpService(afpRepository, afpMapper);
	}
		
	@Test
	void create() throws Exception 
	{
		 //given
		 Afp afp = Afp.builder()
				 	   .id(Long.valueOf(1))
				 	   .description("PRIMA")
				 	   .build();
		 given(afpRepository.save(any(Afp.class))).willReturn(afp);
		 
		 //when		
		 AfpModel afpsToCreate = afpMapper.afpToAfpModel(afp);
		 AfpModel afpsCreated = afpService.create(afpsToCreate);
	     
	     //then
	     then(afpRepository).should().save(any(Afp.class));
	     assertThat(afpsCreated).isNotNull();
	}
	
	@Test
	void update() throws Exception 
	{
		 //given
		 Afp afp = Afp.builder()
			 	   .id(Long.valueOf(1))
			 	   .description("PRIMA")
			 	   .build();
		 given(afpRepository.findById(Long.valueOf(1))).willReturn(Optional.of(afp));
	 
		 //when		
		 AfpModel afpsToUpdate = afpMapper.afpToAfpModel(afp);
		 afpService.update(Long.valueOf(1), afpsToUpdate);
	     
	     //then
	     then(afpRepository).should().save(any(Afp.class));
	}
	
	@Test
	void updateNotFound() throws Exception 
	{
		 Afp afp = Afp.builder()
			 	   .id(Long.valueOf(1))
			 	   .description("PRIMA")
			 	   .build();
		 AfpModel afpsToUpdate = afpMapper.afpToAfpModel(afp);
		    
		 assertThrows(Exception.class, () -> afpService.update(Long.valueOf(1), afpsToUpdate));
	}
		
	@Test
	void findAll() throws Exception 
	{
		 //given
		 List<Afp> afps = new ArrayList<Afp>();
		 afps.add(Afp.builder()
			 	   .id(Long.valueOf(1))
			 	   .description("PRIMA")
			 	   .build());
		 
		 afps.add(Afp.builder()
			 	   .id(Long.valueOf(1))
			 	   .description("PRIMA")
			 	   .build());
		 
		 given(afpRepository.findAll()).willReturn(afps);
		 
		 //when
	     List<AfpModel> afpsFindAll = afpService.findAll();
	     
	     //then
	     then(afpRepository).should().findAll();
	     assertThat(afpsFindAll).isNotNull();
	}
	
	@Test
	void findById() throws Exception 
	{
		 //given
		 Afp afp = new Afp();
		 given(afpRepository.findById(Long.valueOf(1))).willReturn(Optional.of(afp));
		 
		 //when
	     AfpModel afpsFindById = afpService.findById(Long.valueOf(1));
	     
	     //then
	     then(afpRepository).should().findById(Long.valueOf(1));
	     assertThat(afpsFindById).isNotNull();
	}
	
	@Test
	void findByIdNotFound() throws Exception 
	{
		 assertThrows(Exception.class, () -> afpService.findById(Long.valueOf(1)));
	}
			
	@Test
	void deleteById() throws Exception{
		 //given
		 
		 //when		
		afpService.deleteById(Long.valueOf(1));
	     
	     //then
	     then(afpRepository).should().deleteById(anyLong());
	}

}
