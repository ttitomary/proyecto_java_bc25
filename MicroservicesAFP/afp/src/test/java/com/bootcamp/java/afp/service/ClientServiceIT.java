package com.bootcamp.java.afp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import com.bootcamp.java.afp.domain.Client;
import com.bootcamp.java.afp.repository.ClientRepository;
import com.bootcamp.java.afp.service.mapper.ClientMapper;
import com.bootcamp.java.afp.web.model.ClientModel;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@SpringBootTest(classes = {ClientMapper.class})
class ClientServiceIT {

	@Mock
	ClientRepository clientRepository;
	
	@Spy
	private ClientMapper clientMapper = Mappers.getMapper(ClientMapper.class);
	 
	ClientService clientService;
	
	@BeforeEach
	void beforeEach() {
		clientService = new ClientService(clientRepository, clientMapper);
	}
		
	@Test
	void create() throws Exception 
	{
		 //given
		 Client client = Client.builder()
				 	   .id(Long.valueOf(1))
				 	   .name("Mary")
				 	   .address("Ttito")
				 	   .document("75007684")
				 	   .phone("964678226")
				 	   .email("mary.ttito.16@outlook.com")
				 	   .build();
		 given(clientRepository.save(any(Client.class))).willReturn(client);
		 
		 //when		
		 ClientModel clientsToCreate = clientMapper.clientToClientModel(client);
		 ClientModel clientsCreated = clientService.create(clientsToCreate);
	     
	     //then
	     then(clientRepository).should().save(any(Client.class));
	     assertThat(clientsCreated).isNotNull();
	}
	
	@Test
	void update() throws Exception 
	{
		 //given
		 Client client = Client.builder()
				 	   .id(Long.valueOf(1))
				 	   .name("Mary")
				 	   .address("Ttito")
				 	   .document("75007684")
				 	   .phone("964678226")
				 	   .email("mary.ttito.16@outlook.com")
				 	   .build();
		 given(clientRepository.findById(Long.valueOf(1))).willReturn(Optional.of(client));
	 
		 //when		
		 ClientModel clientsToUpdate = clientMapper.clientToClientModel(client);
	     clientService.update(Long.valueOf(1), clientsToUpdate);
	     
	     //then
	     then(clientRepository).should().save(any(Client.class));
	}
	
	@Test
	void updateNotFound() throws Exception 
	{
		Client client = Client.builder()
			 	   .id(Long.valueOf(1))
			 	   .name("Mary")
			 	   .address("Ttito")
			 	   .document("75007684")
			 	   .phone("964678226")
			 	   .email("mary.ttito.16@outlook.com")
			 	   .build();
		 ClientModel clientsToUpdate = clientMapper.clientToClientModel(client);
		    
		 assertThrows(Exception.class, () -> clientService.update(Long.valueOf(1), clientsToUpdate));
	}
		
	@Test
	void findAll() throws Exception 
	{
		 //given
		 List<Client> clients = new ArrayList<Client>();
		 clients.add(Client.builder()
			 	   .id(Long.valueOf(1))
			 	   .name("Mary")
			 	   .address("Ttito")
			 	   .document("75007684")
			 	   .phone("964678226")
			 	   .email("mary.ttito.16@outlook.com")
			 	   .build());
		 
		 clients.add(Client.builder()
			 	   .id(Long.valueOf(1))
			 	   .name("Mary")
			 	   .address("Ttito")
			 	   .document("75007684")
			 	   .phone("964678226")
			 	   .email("mary.ttito.16@outlook.com")
			 	   .build());
		 
		 given(clientRepository.findAll()).willReturn(clients);
		 
		 //when
	     List<ClientModel> clientsFindAll = clientService.findAll();
	     
	     //then
	     then(clientRepository).should().findAll();
	     assertThat(clientsFindAll).isNotNull();
	}
	
	@Test
	void findById() throws Exception 
	{
		 //given
		 Client client = new Client();
		 given(clientRepository.findById(Long.valueOf(1))).willReturn(Optional.of(client));
		 
		 //when
	     ClientModel clientsFindById = clientService.findById(Long.valueOf(1));
	     
	     //then
	     then(clientRepository).should().findById(Long.valueOf(1));
	     assertThat(clientsFindById).isNotNull();
	}
	
	@Test
	void findByIdNotFound() throws Exception 
	{
		 assertThrows(Exception.class, () -> clientService.findById(Long.valueOf(1)));
	}
			
	@Test
	void deleteById() throws Exception{
		 //given
		 
		 //when		
	     clientService.deleteById(Long.valueOf(1));
	     
	     //then
	     then(clientRepository).should().deleteById(anyLong());
	}
	
}
