package com.bootcamp.java.afp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import com.bootcamp.java.afp.domain.Afp;
import com.bootcamp.java.afp.domain.Client;
import com.bootcamp.java.afp.domain.Demand;
import com.bootcamp.java.afp.repository.DemandRepository;
import com.bootcamp.java.afp.service.mapper.DemandMapper;
import com.bootcamp.java.afp.web.model.DemandModel;

@SpringBootTest(classes = {DemandMapper.class})
class DemandServiceIT {

	@Mock
	DemandRepository demandRepository;
	
	@Spy
	private DemandMapper demandMapper = Mappers.getMapper(DemandMapper.class);
	 
	DemandService demandService;
	
	@BeforeEach
	void beforeEach() {
		demandService = new DemandService(demandRepository, demandMapper);
	}
		
	@Test
	void create() throws Exception 
	{
		 //given
		 Afp afp = new Afp();
		 afp.setId(Long.valueOf(1));
		 afp.setDescription("PRIMA");
		 
		 Client client = new Client();
		 client.setId(Long.valueOf(1));
		 client.setName("Mary");
		 client.setAddress("Ttito");
		 client.setDocument("75007684");
		 client.setPhone("964678226");
		 client.setEmail("mary.ttito.16@outlook.com");
		 
		 Demand demand = Demand.builder()
			 	   .id(Long.valueOf(1))
			 	   .afp(afp)
			 	   .client(client)
			 	   .document("75007684")
			 	   .mount(1000)
			 	   .status('1')
			 	   .build();
		 given(demandRepository.save(any(Demand.class))).willReturn(demand);
		 
		 //when		
		 DemandModel demandsToCreate = demandMapper.demandToDemandModel(demand);
		 DemandModel demandsCreated = demandService.create(demandsToCreate);
	     
	     //then
	     then(demandRepository).should().save(any(Demand.class));
	     assertThat(demandsCreated).isNotNull();
	}
	
	@Test
	void update() throws Exception 
	{
		 //given
		 Afp afp = new Afp();
		 afp.setId(Long.valueOf(1));
		 afp.setDescription("PRIMA");
		 
		 Client client = new Client();
		 client.setId(Long.valueOf(1));
		 client.setName("Mary");
		 client.setAddress("Ttito");
		 client.setDocument("75007684");
		 client.setPhone("964678226");
		 client.setEmail("mary.ttito.16@outlook.com");
		 
		 Demand demand = Demand.builder()
				 	   .id(Long.valueOf(1))
				 	   .afp(afp)
				 	   .client(client)
				 	   .document("75007684")
				 	   .mount(1000)
				 	   .status('1')
				 	   .build();
		 given(demandRepository.findById(Long.valueOf(1))).willReturn(Optional.of(demand));
	 
		 //when		
		 DemandModel demandsToUpdate = demandMapper.demandToDemandModel(demand);
		 demandService.update(Long.valueOf(1), demandsToUpdate);
	     
	     //then
	     then(demandRepository).should().save(any(Demand.class));
	}
	
	@Test
	void updateNotFound() throws Exception 
	{
		Afp afp = new Afp();
		 afp.setId(Long.valueOf(1));
		 afp.setDescription("PRIMA");
		 
		 Client client = new Client();
		 client.setId(Long.valueOf(1));
		 client.setName("Mary");
		 client.setAddress("Ttito");
		 client.setDocument("75007684");
		 client.setPhone("964678226");
		 client.setEmail("mary.ttito.16@outlook.com");
		 
		Demand demand = Demand.builder()
			 	   .id(Long.valueOf(1))
			 	   .afp(afp)
			 	   .client(client)
			 	   .document("75007684")
			 	   .mount(1000)
			 	   .status('1')
			 	   .build();
		 DemandModel demandsToUpdate = demandMapper.demandToDemandModel(demand);
		    
		 assertThrows(Exception.class, () -> demandService.update(Long.valueOf(1), demandsToUpdate));
	}
		
	@Test
	void findAll() throws Exception 
	{
		 //given
		 Afp afp = new Afp();
		 afp.setId(Long.valueOf(1));
		 afp.setDescription("PRIMA");
		 
		 Client client = new Client();
		 client.setId(Long.valueOf(1));
		 client.setName("Mary");
		 client.setAddress("Ttito");
		 client.setDocument("75007684");
		 client.setPhone("964678226");
		 client.setEmail("mary.ttito.16@outlook.com");
		 
		 List<Demand> demands = new ArrayList<Demand>();
		 demands.add(Demand.builder()
			 	   .id(Long.valueOf(1))
			 	   .afp(afp)
			 	   .client(client)
			 	   .document("75007684")
			 	   .mount(1000)
			 	   .status('1')
			 	   .build());
		 
		 demands.add(Demand.builder()
			 	   .id(Long.valueOf(1))
			 	   .afp(afp)
			 	   .client(client)
			 	   .document("75007684")
			 	   .mount(1000)
			 	   .status('1')
			 	   .build());
		 
		 given(demandRepository.findAll()).willReturn(demands);
		 
		 //when
	     List<DemandModel> demandsFindAll = demandService.findAll();
	     
	     //then
	     then(demandRepository).should().findAll();
	     assertThat(demandsFindAll).isNotNull();
	}
	
	@Test
	void findById() throws Exception 
	{
		 //given
		 Demand demand = new Demand();
		 given(demandRepository.findById(Long.valueOf(1))).willReturn(Optional.of(demand));
		 
		 //when
	     DemandModel demandsFindById = demandService.findById(Long.valueOf(1));
	     
	     //then
	     then(demandRepository).should().findById(Long.valueOf(1));
	     assertThat(demandsFindById).isNotNull();
	}
	
	@Test
	void findByIdNotFound() throws Exception 
	{
		 assertThrows(Exception.class, () -> demandService.findById(Long.valueOf(1)));
	}
			
	@Test
	void deleteById() throws Exception{
		 //given
		 
		 //when		
		demandService.deleteById(Long.valueOf(1));
	     
	     //then
	     then(demandRepository).should().deleteById(anyLong());
	}
}
