package com.bootcamp.java.afp.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import com.bootcamp.java.afp.domain.Afp;
import com.bootcamp.java.afp.domain.Client;
import com.bootcamp.java.afp.domain.Detail;
import com.bootcamp.java.afp.repository.DetailRepository;
import com.bootcamp.java.afp.service.mapper.DetailMapper;
import com.bootcamp.java.afp.web.model.DetailModel;

@SpringBootTest(classes = {DetailMapper.class})
class DetailServiceIT {

	@Mock
	DetailRepository detailRepository;
	
	@Spy
	private DetailMapper detailMapper = Mappers.getMapper(DetailMapper.class);
	 
	DetailService detailService;
	
	@BeforeEach
	void beforeEach() {
		detailService = new DetailService(detailRepository, detailMapper);
	}
		
	@Test
	void create() throws Exception 
	{
		 //given
		 Afp afp = new Afp();
		 afp.setId(Long.valueOf(1));
		 afp.setDescription("PRIMA");
		 
		 Client client = new Client();
		 client.setId(Long.valueOf(1));
		 client.setName("Mary");
		 client.setAddress("Ttito");
		 client.setDocument("75007684");
		 client.setPhone("964678226");
		 client.setEmail("mary.ttito.16@outlook.com");
		 
		 Detail detail = Detail.builder()
			 	   .id(Long.valueOf(1))
			 	   .afp(afp)
			 	   .client(client)
			 	   .document("75007684")
			 	   .mount(1000)
			 	   .retirement_date(Timestamp.from(Instant.now()))
			 	   .account_number("1981989")
			 	   .build();
		 given(detailRepository.save(any(Detail.class))).willReturn(detail);
		 
		 //when		
		 DetailModel detailsToCreate = detailMapper.detailToDetailModel(detail);
		 DetailModel detailsCreated = detailService.create(detailsToCreate);
	     
	     //then
	     then(detailRepository).should().save(any(Detail.class));
	     assertThat(detailsCreated).isNotNull();
	}
	
	@Test
	void update() throws Exception 
	{
		 //given
		 Afp afp = new Afp();
		 afp.setId(Long.valueOf(1));
		 afp.setDescription("PRIMA");
		 
		 Client client = new Client();
		 client.setId(Long.valueOf(1));
		 client.setName("Mary");
		 client.setAddress("Ttito");
		 client.setDocument("75007684");
		 client.setPhone("964678226");
		 client.setEmail("mary.ttito.16@outlook.com");
		 
		 Detail detail = Detail.builder()
				 	   .id(Long.valueOf(1))
				 	   .afp(afp)
				 	   .client(client)
				 	   .document("75007684")
				 	   .mount(1000)
				 	   .retirement_date(Timestamp.from(Instant.now()))
				 	   .account_number("1981989")
				 	   .build();
		 given(detailRepository.findById(Long.valueOf(1))).willReturn(Optional.of(detail));
	 
		 //when		
		 DetailModel detailsToUpdate = detailMapper.detailToDetailModel(detail);
		 detailService.update(Long.valueOf(1), detailsToUpdate);
	     
	     //then
	     then(detailRepository).should().save(any(Detail.class));
	}
	
	@Test
	void updateNotFound() throws Exception 
	{
		Afp afp = new Afp();
		 afp.setId(Long.valueOf(1));
		 afp.setDescription("PRIMA");
		 
		 Client client = new Client();
		 client.setId(Long.valueOf(1));
		 client.setName("Mary");
		 client.setAddress("Ttito");
		 client.setDocument("75007684");
		 client.setPhone("964678226");
		 client.setEmail("mary.ttito.16@outlook.com");
		 
		Detail detail = Detail.builder()
				 	   .id(Long.valueOf(1))
				 	   .afp(afp)
				 	   .client(client)
				 	   .document("75007684")
				 	   .mount(1000)
				 	   .retirement_date(Timestamp.from(Instant.now()))
				 	   .account_number("1981989")
				 	   .build();
		 DetailModel detailsToUpdate = detailMapper.detailToDetailModel(detail);
		    
		 assertThrows(Exception.class, () -> detailService.update(Long.valueOf(1), detailsToUpdate));
	}
		
	@Test
	void findAll() throws Exception 
	{
		 //given
		 Afp afp = new Afp();
		 afp.setId(Long.valueOf(1));
		 afp.setDescription("PRIMA");
		 
		 Client client = new Client();
		 client.setId(Long.valueOf(1));
		 client.setName("Mary");
		 client.setAddress("Ttito");
		 client.setDocument("75007684");
		 client.setPhone("964678226");
		 client.setEmail("mary.ttito.16@outlook.com");
		 
		 List<Detail> details = new ArrayList<Detail>();
		 details.add(Detail.builder()
			 	   .id(Long.valueOf(1))
			 	   .afp(afp)
			 	   .client(client)
			 	   .document("75007684")
			 	   .mount(1000)
			 	   .retirement_date(Timestamp.from(Instant.now()))
			 	   .account_number("1981989")
			 	   .build());
		 
		 details.add(Detail.builder()
			 	   .id(Long.valueOf(1))
			 	   .afp(afp)
			 	   .client(client)
			 	   .document("75007684")
			 	   .mount(1000)
			 	   .retirement_date(Timestamp.from(Instant.now()))
			 	   .account_number("1981989")
			 	   .build());
		 
		 given(detailRepository.findAll()).willReturn(details);
		 
		 //when
	     List<DetailModel> detailsFindAll = detailService.findAll();
	     
	     //then
	     then(detailRepository).should().findAll();
	     assertThat(detailsFindAll).isNotNull();
	}
	
	@Test
	void findById() throws Exception 
	{
		 //given
		 Detail detail = new Detail();
		 given(detailRepository.findById(Long.valueOf(1))).willReturn(Optional.of(detail));
		 
		 //when
	     DetailModel detailsFindById = detailService.findById(Long.valueOf(1));
	     
	     //then
	     then(detailRepository).should().findById(Long.valueOf(1));
	     assertThat(detailsFindById).isNotNull();
	}
	
	@Test
	void findByIdNotFound() throws Exception 
	{
		 assertThrows(Exception.class, () -> detailService.findById(Long.valueOf(1)));
	}
			
	@Test
	void deleteById() throws Exception{
		 //given
		 
		 //when		
		detailService.deleteById(Long.valueOf(1));
	     
	     //then
	     then(detailRepository).should().deleteById(anyLong());
	}
}
