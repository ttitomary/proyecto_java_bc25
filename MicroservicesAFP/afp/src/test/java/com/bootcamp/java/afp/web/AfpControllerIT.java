package com.bootcamp.java.afp.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.bootcamp.java.afp.service.IAfpService;
import com.bootcamp.java.afp.web.model.AfpModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@AutoConfigureMockMvc
@WebMvcTest(AfpController.class)
class AfpControllerIT {

	@MockBean
	private IAfpService afpService;

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void getAll() throws Exception {
		mockMvc.perform(get("/v1/afp").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	void getById() throws Exception {
		mockMvc.perform(get("/v1/afp/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	void create() throws Exception {
		AfpModel afp = AfpModel.builder().id(Long.valueOf(1)).description("PRIMA").build();

		mockMvc.perform(MockMvcRequestBuilders.post("/v1/afp").content(asJsonString(afp))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	void update() throws Exception {
		AfpModel afp = AfpModel.builder().id(Long.valueOf(1)).description("PRIMA").build();

		mockMvc.perform(MockMvcRequestBuilders.put("/v1/afp/1").content(asJsonString(afp))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	void deleteById() throws Exception {
		mockMvc.perform(delete("/v1/afp/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	public static String asJsonString(final Object obj) throws JsonProcessingException {
		return new ObjectMapper().writeValueAsString(obj);
	}

}
