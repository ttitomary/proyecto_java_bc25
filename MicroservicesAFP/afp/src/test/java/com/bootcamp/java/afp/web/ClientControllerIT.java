package com.bootcamp.java.afp.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.bootcamp.java.afp.service.IClientService;
import com.bootcamp.java.afp.web.model.ClientModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@AutoConfigureMockMvc
@WebMvcTest(ClientController.class)
class ClientControllerIT {
	
	@MockBean
	private IClientService clientService;

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void getAll() throws Exception {
		mockMvc.perform(get("/v1/client").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	void getById() throws Exception {
		mockMvc.perform(get("/v1/client/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	void create() throws Exception {
		ClientModel client = ClientModel.builder().id(Long.valueOf(1))
				.name("Mary").address("Ttito").document("75007684")
				.phone("964678226").email("mary.ttito.16@outlook.com").build();

		mockMvc.perform(MockMvcRequestBuilders.post("/v1/client").content(asJsonString(client))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	@Test
	void update() throws Exception {
		ClientModel client = ClientModel.builder().id(Long.valueOf(1))
				.name("Mary").address("Ttito").document("75007684")
				.phone("964678226").email("mary.ttito.16@outlook.com").build();

		mockMvc.perform(MockMvcRequestBuilders.put("/v1/client/1").content(asJsonString(client))
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	@Test
	void deleteById() throws Exception {
		mockMvc.perform(delete("/v1/client/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	public static String asJsonString(final Object obj) throws JsonProcessingException {
		return new ObjectMapper().writeValueAsString(obj);
	}
}
